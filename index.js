console.log("Hello Again 204");

	/*
		3. Create a function which will be able to add two numbers.
			○ Numbers must be provided as arguments.
			○ Display the result of the addition in our console.
			○ function should only display result. It should not return anything.
			Invoke and pass 2 arguments to the addition function
	*/

	function sumOfNumbers(sum1, sum2) {
		console.log("Displayed sum of " + sum1 + " and " + sum2);
		// let num1 = 15;
		// let num2 = 20;
		// console.log(num1 + num2);
		console.log(sum1 + sum2);
	}
	sumOfNumbers(5, 15);
// =========================================================================
	/*
		4. Create a function which will be able to subtract two numbers.

			○ Numbers must be provided as arguments.
			○ Display the result of subtraction in our console.
			○ function should only display result. It should not return anything.
			Invoke and pass 2 arguments to the subtraction function
	*/
	function differenceOfNumbers(difference1, difference2) {
		console.log("Displayed product of " + difference1 + " and " + difference2);
		// let num1 = 20;
		// let num2 = 15;
		// console.log(num1 + num2);
		console.log(difference1 - difference2);
	}
	differenceOfNumbers(20, 5);
// =========================================================================
	/*
		7. Create a function which will be able to multiply two numbers.

			○ Numbers must be provided as arguments.
			○ Return the result of the multiplication.
		9. Create a new variable called product.
			○ This product variable should be able to receive and store the result of
			multiplication function.
	*/
	function productOfNumber(product1, product2) {
		console.log("The product of " + product1 + " and " + product2 + ":");
		return(product1 * product2);
	} 
	let product = productOfNumber(50, 10);
	console.log(product);
// =========================================================================
	/*
		8. Create a function which will be able to divide two numbers.
			○ Numbers must be provided as arguments.
			○ Return the result of the division.
		10. Create a new variable called quotient.

			○ This quotient variable should be able to receive and store the result of division
			function.
			○ Log the value of product variable in the console.
			○ Log the value of quotient variable in the console.
*/	
	
	function quotientOfNumber(quotient1, quotient2) {
		console.log("The quotient of " + quotient1 + " and " + quotient2 + ":");
		return(quotient1 / quotient2);
	} 
	let quotient = quotientOfNumber(50, 10);
	console.log(quotient);
// =========================================================================
/*
		11. Create a function which will be able to get total area of a circle from a
			provided radius.
			○ a number should be provided as an argument.
			○ look up the formula for calculating the area of a circle with a provided/given radius.
			○ look up the use of the exponent operator.
			○ return the result of the area calculation.
		12. Create a new variable called circleArea.
			○ This variable should be able to receive and store the result of the circle area
			calculation.
			○ Log the value of the circleArea variable in the console.
*/
	function areaOfCircle(pi, radius, squared) {
		console.log("The result of getting the area of a circle with " + radius + " radius:");
		return(pi*radius**squared);
	}
	let circleArea = areaOfCircle(3.14, 15, 2);
	console.log(circleArea);
// =========================================================================

/*
		13. Create a function which will be able to get total average of four
			numbers.
			○ 4 numbers should be provided as an argument.
			○ look up the formula for calculating the average of numbers.
			○ return the result of the average calculation.
		14. Create a new variable called averageVar.
			○ This variable should be able to receive and store the result of the average
			calculation
			○ Log the value of the averageVar variable in the console.
*/
	function average(score1, score2, score3, score4, divisor) {
		// let divisor = 4;
		console.log('The average of ' + " " + score1 + " " +  score2 + " " + score3 + " and " + score4 + ":");
		return((score1 + score2 + score3 + score4) / 4);
	}
	let averageVar = average(20, 40, 60, 80,);
	console.log(averageVar);
// =========================================================================
	
/*
		15. Create a function which will be able to check if you passed by
			checking the percentage of your score against the passing
			percentage.
			○ this function should take 2 numbers as an argument, your score and the total
			score.
			○ First, get the percentage of your score against the total. You can look up the formula
			to get percentage.
			○ Using a relational operator, check if your score percentage is greater than or equal
			to 75, the passing percentage. Save the value of the comparison in a variable called
			isPassed.
			○ return the value of the variable isPassed.
			○ This function should return a boolean.
		16. Create a new variable called isPassingScore.

			○ This variable should be able to receive and store the boolean result of the function.
			○ Log the value of the isPassingScore variable in the console.
*/
// =========================================================================
function passingScore(score, totalScore, percentage, isPassingScore) {
	console.log("is " + " " + score +"/"+ totalScore +" a passing score?");
	let isPassed = 75;
	let myScore = 76;
	
	return(myScore >= isPassed);
}
let isPassingScore = passingScore(38, 50,);
console.log(isPassingScore);
